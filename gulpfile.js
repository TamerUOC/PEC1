/**
 * Created by TamerAdministrador on 20/3/16.
 *
 * Otros plugins interesantes:
 *
 * gulp-del (parar borrar archivos)
 * gulp-order (para definir orden de procesamiento de los CSS
 * run-sequence (para asegurar la ejecucion de procesos dependientes de manera secuencial
 */

var gulp = require('gulp'),
    gJshint =           require('gulp-jshint'),             // Lint/Hint de JS
    gJshintStylish =    require('jshint-stylish'),          // Mejorar estilo de presentacion de Lint
    gConcat =           require('gulp-concat'),             // Concatenar archivos
    gUglify =           require('gulp-uglify'),             // Minificador JS
    gMinify =           require('gulp-clean-css'),          // Minificador CSS
    gSass =             require('gulp-sass'),               // Preprocesador CSS
    gSrcMaps =          require('gulp-sourcemaps'),         // SourceMaps CSS
    gBrowserSync =      require('browser-sync').create(),   // BrowserSync
    gPlumber =          require('gulp-plumber');            // Error-stopper-prevent durante la ejecucion
    paths = {
                        jsOrig : 'src/js/*.js',
                        jsDest : 'dist/js',
                        cssOrig : 'src/sass/*.scss',
                        cssDest : 'dist/css'
    };

gulp.task('default',['tPublicacion']);

gulp.task('tJshint', function () {
   return gulp.src(paths.jsOrig)
       .pipe(gPlumber())
       .pipe(gJshint())
       .pipe(gJshint.reporter(gJshintStylish));
});

gulp.task('tUglify', function () {
    return gulp.src(paths.jsOrig)
        .pipe(gPlumber())
        .pipe(gUglify())
        .pipe(gConcat('pec1-scripts.js'))
        .pipe(gulp.dest(paths.jsDest))
        .pipe(gBrowserSync.stream());
});

gulp.task('tSass', function () {
    return gulp.src([paths.cssOrig,'!src/sass/variables.scss'])
        .pipe(gSrcMaps.init())
        .pipe(gSass().on('error', gSass.logError))
        .pipe(gMinify())
        .pipe(gConcat('pec1-styles.css'))
        .pipe(gSrcMaps.write('./'))
        .pipe(gulp.dest(paths.cssDest))
        .pipe(gBrowserSync.stream());
});

gulp.task('tPublicacion', ['tSass','tJshint','tUglify'], function() {
    gBrowserSync.init({
        proxy   : "http://localhost/UOC-PEC/PEC1.Ej5/dist/"
    });
    gulp.watch(paths.cssOrig,['tSass']);
    gulp.watch(paths.jsOrig,['tJshint','tUglify']);
    gulp.watch("dist/*.html").on('change', gBrowserSync.reload);
});

gulp.task('tPublicacionEstatica', ['tSass','tJshint','tUglify'], function() {
    gBrowserSync.init({
        server: {
            baseDir: "dist/"
        }
    });
    gulp.watch(paths.cssOrig,['tSass']);
    gulp.watch(paths.jsOrig,['tJshint','tUglify']);
    gulp.watch("dist/*.html").on('change', gBrowserSync.reload);
});

gulp.task('tWatch', function () { // realizar Watch sin BrowserSync
    gulp.watch(paths.cssOrig,['tSass']);
    gulp.watch(paths.jsOrig,['tJshint']);
    gulp.watch(paths.jsOrig,['tUglify']);
});